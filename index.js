const { request, Router } = require("express");
const express = require("express");
const mongoose = require("mongoose");

const app = express();
// routers
const courseRouter = require("./app/routes/courseRouter")
const reviewRouter = require("./app/routes/reviewRouter")
// models
const reviewModel = require("./app/models/reviewModel")
const courseModel = require("./app/models/courseModel")

app.use(function(request, response, next){
    console.log("Current time: ", new Date());
    next();
})
app.use(function(request, response, next) {
    console.log("Request method: ", request.method);
    next();
})

app.use("/api", courseRouter);
app.use("/api", reviewRouter);

// get method
app.get("/", function(request, response) {
    response.json({
        message: "Devcamp Middleware Express APP"
    })
})

// kết nối mongodb
mongoose.connect('mongodb://127.0.0.1:27017/CRUD_Course', function(error) {
    if (error) throw error;
    console.log('Successfully connected');
   })
   
// port
const port = 8000;
app.listen(port, function() {
    console.log(`App running on port ${port}`);
})