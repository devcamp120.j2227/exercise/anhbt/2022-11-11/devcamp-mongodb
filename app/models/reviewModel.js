const mongoose = require("mongoose");
const Schema = mongoose.Schema;

const reviewSchema = new Schema({
    stars:{
        type: Number,
        default: 0
    },
    note:{
        type: String,
        require: false
    }
}, {
    // ghi dấu bản ghi cập nhật thời gian nào
    timestamps: true
})

module.exports = mongoose.model("Review", reviewSchema)